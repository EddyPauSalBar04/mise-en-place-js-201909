/**
 * This sample show how to use the high order function  map in arrays.
 *  Follow the steps.  Read,  understand, execute, reply, modified
 *  First we define and array of animals and we need generate new arrays.
 *  The sample shows you different way to get these new arrays  
 */

/**
 * Step 1:   Define an array 
 */
const animals = [
    { "name" : "Fido", "species": "Dog"  , "race": "Chihuahua" },
    { "name" : "Fifi", "species": "Dog"  , "race": "Poodle" },
    { "name" : "Wafles", "species": "Cat"  , "race": "Persian Cat" },
    { "name" : "Barracuda", "species": "Fish"  , "race": "Betha" }
];

console.log("Step 01: Printing all animals");
console.table( animals );


/**
 * Step  2:  Get the animal names using a tradicional for statement
 * Define the size to know how many iterations will be.
 * Define and array to get the animal name 
 * After the itereation we send to console, each array.
 * 
 */
const animalsSize = animals.length;

const animalNamesFor =[];

for( let index= 0 ; index < animalsSize; index++ ){
    
    animalNamesFor.push( animals[index].name  ); 
    
}

console.log("Step 02: Print the names get by For");
console.table(  animalNamesFor );



/**
 * Step 3 :  Using the map high order function, get the animals names.
 * The map funcion needs another function.
 * For each element in animals array, will execute the function (in this case an anonymous function)
 * At the end of iteration, we will get an array of string with the animal names
 * This is the function used in map method:
 * 
 * function( animal ){ 
 *  return animal.name;
 * }
 * 
 * The function must return a string value * 
 */
const anonymousNames = animals.map( function( animal ){ return animal.name; } );

console.log( "Step 3: get names by anonymous function" );
console.table(  anonymousNames  );


/**
 * Step 4: Using the map high order function,to get the animal names.
 * In this scenario we will use an arrow function, equivalent to the previous step
 * 
 */
const arrowNames = animals.map( animal => animal.name );

console.log( "Step 4: get names by arrow function " );
console.table( arrowNames );


/**
 * Step 5: We define a callback ( a function, that other function will use )
 * The callback is declare like a variable, the function is identify by the name getNamesCallBack
 * @param {*} animal  
 */
const getNamesCallBack = function( animal ){
    return animal.name;
}

/**
 * Step 5.1:  We use the map function, but this time we call the previous function declared
 * The callback ( the function declare like a variable with the name  getNamesCallBack ) is used by the map.
 */
const namesByCallBack = animals.map( getNamesCallBack );

console.log("Step 5 print names By callBack: " );
console.table( namesByCallBack );



/**
 * Step 6: We define a callback named getNamesArrowCB
 * This callback is defined like an arrow function.
 * @param {*} animal 
 */
const getNamesArrowCB = animal  => animal.name;


/**
 * Step 6.1  we use the callback getNamesArrowCB  in the map to get the names
 */
const namesByArrowCB = animals.map( getNamesArrowCB );

console.log( "Step 6, get names By arrow callback: " );
console.table(  namesByArrowCB );


/**
 * Step 7  we use and arrow function to get a label with animal attributes
 */
const animalsAtributsArrow = animals.map( animal =>  `The ${animal.species} named ${animal.name} is a ${animal.race}`   );

console.log( "Step 7, Other animals " );
console.table(  animalsAtributsArrow );


/**
 * Step 8 We define another function that deconstruct an object in variables
 * 
 * The origina  code, we call object.attribute...
 * `The ${animal.species} named ${animal.name} is a ${animal.race}`
 *  
 * With deconstruction we generate variables from the object.* 
 * 
 * 
 * @param {*} animal 
 */
const deconstructArrow = animal => {
    const { name, species, race } = animal;

    //     `The ${animal.species} named ${animal.name} is a ${animal.race}`
    return `The ${species} named ${name} is a ${race}`;

};

/**
 * Step 8.1 we use the new callback to get animals that are not dogs
 */
const labelArray = animals.map( deconstructArrow );

console.log( "Step 8, Labels by deconstruct objects" );
console.table(  labelArray );

