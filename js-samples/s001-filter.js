/**
 * This sample show how to use the high order function  filter in arrays.
 *  Follow the steps.  Read,  understand, execute, reply, modified
 *  First we define and array of animals and we need filter the animals that are dog.
 *  The sample shows you different way to get the dogs and the animals that arent dog  
 */



/**
 * Step 1:   Define an array 
 */
const animals = [
    { "name" : "Fido", "species": "Dog"  , "race": "Chihuahua" },
    { "name" : "Fifi", "species": "Dog"  , "race": "Poodle" },
    { "name" : "Wafles", "species": "Cat"  , "race": "Persian Cat" },
    { "name" : "Barracuda", "species": "Fish"  , "race": "Betha" }
];

console.log("Step 01: Printing all animals");
console.table( animals );


/**
 * Step  2:  Get the dogs using a tradicional for statement
 * Define the size to know how many iterations will be.
 * Define and array to get the dogs and another one to get the animals that are not dogs
 * Inside the for we combine an if-else statement to kwon in what array, push the animal.
 * After the itereation we send to console, each array.
 * 
 */
const animalsSize = animals.length;

const dogsFor =[];
const notDogsFor = [];

for( let index= 0 ; index < animalsSize; index++ ){
    if(  animals[index].species == "Dog"  ){
        dogsFor.push( animals[index] ); 
    }else {
        notDogsFor.push( animals[index]  );
    }
}

console.log("Step 02: Print the dogs get by For");
console.table(  dogsFor );

console.log("Step 02: Print the animals that are not dogs, get by For");
console.table( notDogsFor );



/**
 * Step 3 :  Using the filter high order function, get the animals that are dogs.
 * The filter funcion needs another function.
 * For each element in animals array, will execute the function (in this case an anonymous function)
 * At the end of iteration, we will get an array of animals with species is equals to Dog
 * This is the function used in filter method:
 * 
 * function( animal ){ 
 *  return animal.species == 'Dog';
 * }
 * 
 * The function must return and a boolean value (true or false)
 * If the evaluation is true, the element will be push into the array
 * If the evaluation is false, the element is ignored
 * 
 */
const anonymousDogs = animals.filter( function( animal ){ return animal.species == 'Dog'; } );
console.log( "Step 3: get dogs by anonymous function" );
console.table(  anonymousDogs  );



/**
 * Step 4: Using the filter high order function,to get the animals that are dogs.
 * In this scenario we will use an arrow function, equivalent to the previous step
 * 
 */
const arrowDogs = animals.filter( animal => animal.species == 'Dog' );

console.log( "Step 4: get dogs by arrow function " );
console.table( arrowDogs );



/**
 * Step 5: We define a callback ( a function, that other function will use )
 * The callback is declare like a variable, the function is identify by the name getDogsCallBack
 * @param {*} animal  
 */
const getDogsCallBack = function( animal ){
    return animal.species == 'Dog';
}

/**
 * Step 5.1:  We use the filter function, but this time we call the previous function declared
 * The callback ( the function declare like a variable with the name  getDogsCallBack ) is used by the filter.
 */
const dogsByCallBack = animals.filter( getDogsCallBack );

console.log("Step 5 print dogs By callBack: " );
console.table( dogsByCallBack );

/**
 * Step 6: We define a callback named getDogsArrowCB
 * This callback is defined like an arrow function.
 * @param {*} animal 
 */
const getDogsArrowCB = animal  => animal.species == 'Dog';


/**
 * Step 6.1  we use the callback getDogsArrowCB in the filter to get the dogs
 */
const dogsByArrowCB = animals.filter( getDogsArrowCB );

console.log( "Step 6, get dogs By arrow callback: " );
console.table(  dogsByArrowCB );


/**
 * Step 7  we use and arrow function to get animasl diffent to dog
 */
const notDogsArrow = animals.filter( animal => animal.species != 'Dog' );

console.log( "Step 7, Other animals " );
console.table(  notDogsArrow );

/**
 * Step 8 We define another function that re-use getDogsArrowCB, but we change the result
 * 
 * this is  the equivalent
 * 
 * function notDogsArrowCB( animal ){
 *      return  !getDogsArrowCB(animal);
 * }
 * 
 * 
 * @param {*} animal 
 */
const notDogsClosure = animal => !getDogsArrowCB(animal);

/**
 * Step 8.1 we use the new callback to get animals that are not dogs
 */
const notDogs = animals.filter( notDogsClosure );

console.log( "Step 8, Other animals  get by closure" );
console.table(  notDogs );
