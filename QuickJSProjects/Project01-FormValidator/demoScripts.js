
const  formDemo = document.getElementById('formDemo');
const  userDemo =  document.getElementById('usernameDemo');
const  emailDemo =   document.getElementById('emailDemo');
const  passwordDemo = document.getElementById('passwordDemo');
const  password2Demo = document.getElementById('password2Demo');


document.getElementById('errorBtn').addEventListener('click', function(e){
    userDemo.parentElement.className= "form-control error";
    emailDemo.parentElement.className= "form-control error";
    passwordDemo.parentElement.className= "form-control error";
    password2Demo.parentElement.className= "form-control error";
});

document.getElementById('successBtn').addEventListener('click', e=> {

    const inputArray = [ userDemo, emailDemo, passwordDemo, password2Demo ];

    inputArray.forEach( input=>{
        input.parentElement.className= "form-control success";
    });    
});


formDemo.addEventListener('submit', function(event){
    event.preventDefault();

    const inputArray = [ userDemo, emailDemo, passwordDemo, password2Demo ];

    inputArray.forEach(function(input){
        input.parentElement.className= "form-control";
    });
});



