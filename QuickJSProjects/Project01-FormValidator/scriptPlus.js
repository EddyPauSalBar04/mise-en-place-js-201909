
/**
 * Create a const variable for the next elements. Get the elements by thier id
 * form,  username, email, password, password2
 * 
 */


/**
 * Show input error message 
 * To the parent elment of the input, append the class error.
 * Get the small element with a query Selector.
 * To the small item, use the function  innerText to put the message.
 * 
 * @param {any} input   HTML Element: The input html element,
 * @param {string} message String:  The error message to display
 */
function showError( ){
}

/**
 * Show success outline
 * Th the parent elment of the input, append the class success.
 * @param {any} input HTML: Element: The input html element
 */
function showSuccess(){       
}


/**
 * The function returns  true or false to validate an emai is correct.
 * @param {string} email  The string value to evaluate if its a valid email.
 * @returns {boolean}     The true or false value, depending of the evaluation    
 *  
 */
function isValidEmail(){
}

/**
 * Function tha evaluate if the email is correct.
 * If email is correct, call the showSuccess function.
 * If email is incorrect, call the showError function.
 * 
 * To evaluate if the mail is correct (true) or incorrect (false), call the function isValidEmail 
 * @param {string} email 
 */
function checkEmail(){
}




/**
 * Function to get a String name to put in messages. 
 * The function receives an input html. Take the id of this element.
 * Take the value of the id. Take the first letter and apply and uppercase. 
 * Take the rest of the id value and concat.
 * 
 * @param   {any} input  HTML Elment to get the id
 * @returns {sting}    The String value Example id = "password" the function returns  "Pasword"  
 */
function getFieldName(){
}


/**
 * Function to check if the input is required
 * The function iterate the array, evaluate if its empty or not
 * If the input its empty execute showError.  Use the getFieldName function to get the name to send in the message.
 * If the input its not empy execute showSuccess.
 * 
 * @param {any} inputArray An array of input html elments.
 */
function checkRequired(){
}

/**
 * Function that evaluates if some input value has the min lenght required and the max length required by the field
 * If the value is less than min required, send the message warning the user. Example: Pasword must be at least 6 characteres
 * If the value is more than max required, send the message warning the user. Example: Password must be less than 20 characteres.
 * If the input length is in the range, execute the showSuccess funcion.
 * 
 * @param {any} input 
 * @param {number} min 
 * @param {number} max 
 */
function checkLength(){
}


/**
 * Function that evaluates if two passwords are equal or not.
 * If the two input values are different, send the message: Password do not match. On input 2
 * If the tow input values are equal, execute showSucces on input2.
 * 
 * @param {any} input1 The HTML element with the password
 * @param {any} input2 The HTML element with the password2
 */
function checkPasswordMatch(){
}


/**
 * After execute e.preventDefault() , statr to evaluate each field
 * If the input value is empty (value === '') execute showError on input
 * otherwise execute showSuccess on input
 * 
 * ################################################################################
 * Instructions
 * 1) checkRequired : Check and array of inputs, and for each one send the message
 * 1.1 ) If the input is empty send message [ INPUT_NAME ] is required. Example   Username is required
 * 1.2 ) If the input is not empty send showSuccess 
 *  
 * 2) checkLength on username
 * 2.1 ) If the length of password is less than 3 send message : Username must be at least 3 characters
 * 2.2 ) If the length of password is more than 15 send message : Username must be at less than 15 characters
 * 2.3 ) If the length of password is in range , showSuccess
 * 
 * 3) checkLength: on password
 * 3.1 ) If the length of password is less than 6 send message : Password must be at least 6 characters
 * 3.2 ) If the length of password is more than 25 send message : Password must be at less than 25 characters
 * 3.3)  If the length of password is in range , showSuccess
 * 
 * 4) checkEmail
 * 4.1 ) If the email is not valid, send message: Email is not valid
 * 4.2 ) If the email is valid, send  showSucces on email input
 * 
 * 5) checkPasswordMatch
 * 5.1 )  If the password and password2 are not equal send message: Passwords do not match
 * 5.2 )  If the passwords match, send showSuccess on password2
 * 
 * ################################################################################
 * 
 */
form.addEventListener( 'submit', function( e ){
    e.preventDefault();
    
    checkRequired([ username, email, pasword, password2 ]);

    checkLength(username, 3, 15);

    checkLength(pasword, 6, 25);

    checkEmail(email);

    checkPasswordMatch(pasword, password2);
    
});

